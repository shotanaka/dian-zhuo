/*
 * Main.java
 * ！！！！注意！！！！
 * これまでの授業で学習したオブジェクト指向の知識を総動員して，このソースコードを出来る限り改良してください．
 * クラスを新しく作る場合には，ee57073（ee + 自分の学籍番号の下5桁）パッケージの直下に配置してください．
 * このプログラムを改良する際には，同じ入力に対して，改良後の出力が改良前の出力とまったく同じになるようにしてください．
 * 自分の改良した電卓が改良前の電卓と同様の動作をすることを，各自で十分に確認してください．
 * 念のための注意ですが，電卓プログラム課題としての意図に反するような変更をしてはいけません．
 */
package main;
import ee57073.*;
import oop.*;


import java.util.Scanner; // 式の読み込みに使います

/**
 * ぼくのかんがえたさいきょうのでんたく．
 * 【仕様】
 * 基本的には一般的な電卓と同じものとするが，以下のような例外が存在する．
 * （1）数値と演算子とを必ず半角空白で区切らなければならない．
 *      良い例： 1 + 2 + 3
 *      悪い例： 1+2+3
 * （2）整数型のみをサポートする．実数型はない．
 *      10 / 3 = 3 // ← 実数型がないので，3.33……とはならず3になる．
 * （3）演算子の優先順位はすべて等しく，式は左から右へと計算される．
 *      1 + 2 * 3 = 9 // ← 演算子の優先順位が同じため，(1 + 2) * 3 として計算されるため．
 * （4）一般的な電卓には存在しない演算子が存在する．
 *      10 % 3 = 1 // ← 10を3で割った余り
 *      2 ** 3 = 8 // ← 2の3乗
 * （5）終了時にはEOFを入力する．すなわち，UNIX系OSならば Ctrl+D，Windowsならば Ctrl+Z である．
 * 【使用方法】
 * java main.Main
 * ※javaのオプション指定は省略しています．
 * @author OOP_TA
 * @version 1.0.0
 */
public final class Main implements Runnable{

	
	/**
	 * コンストラクタ
	 */
	public Main(){
		
	}
	
	/**
	 * { @inheritDoc }
	 */
	@Override
	public void run(){
		Scanner scanner = new Scanner(System.in);   // 式の読み込みに使う
        if (!scanner.hasNextInt()) {                // 次の入力が整数値でなければ
            System.err.println("整数値が必要です"); // エラー表示
            System.exit(1);                         // 異常終了
        }
        int left = scanner.nextInt();                   // 最初の整数値を読み込む
        while (scanner.hasNext()) {                     // まだ式があれば
            String operator = scanner.next();           // 演算子を読み込む
            if (!scanner.hasNextInt()) {                // 次の入力が整数値でなければ
                System.err.println("整数値が必要です"); // エラー表示
                System.exit(1);                         // 異常終了
            }
            int right = scanner.nextInt(); // 次の整数値を読み込む
        	ee57073.Calculate cal = new ee57073.Calculate(left,right,operator); //不変クラスを呼び込む。
        	left = cal.getResult();
        }
		 System.out.println(left); // 結果を表示する
	}

	/**
     * mainメソッド
     * @param args args
     */
	 public static void main(final String[] args) {
	 	assert(args != null && args.length == 0);
	 	final Main main = new Main();
	 	main.run();
	 }

}
