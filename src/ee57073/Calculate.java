/*
 * Calculate.java
 */
package ee57073;
import oop.BinaryOperator;
import ee57073.*;
import main.Main;


/**
 * @author 田中 翔
 * @version 1.0.0
 */
public final class Calculate{

	/**
	 * left
	 */
	private final int left;
	/**
	 * right
	 */
	private final int right;
	/**
	 * operate
	 */
	private final String operate;
	
	/**
	 * コンストラクタ
	 * @param left left
	 * @param right right
	 * @param operate operate
	 */
	public Calculate(int left,int right,String operate){
		this.left = left;
		this.right = right;
		this.operate = operate;
	}
	
	/**
	 * getResultメソッド
	* @return Calculate Calculate
	 */
	public int getResult(){
		return Calculate();
	}
	
	/**
	 * Calculateメソッド
	 *@return 演算結果 演算結果
	 */
	private int Calculate(){
		switch(operate){
		case "+":
			BinaryOperator add = new Add();//不変クラスの委譲
			return add.operate(left,right);
		case "-":
			oop.BinaryOperator sub = new Sub();
			return sub.operate(left,right);
		case "*":
			BinaryOperator mul = new Mul();
			return mul.operate(left,right);
		case "/":
			BinaryOperator div = new Div();
			return div.operate(left,right);
		case "**":
			BinaryOperator power = new Power();
			return power.operate(left,right);
		default:                   // エラー
			System.err.printf("未知の演算子です：%s%n", operate);
			System.exit(1);	
		}
		return 0;
	}
	


}