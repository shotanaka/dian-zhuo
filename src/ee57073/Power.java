/*
 * Power.java
 */
package ee57073;
import oop.BinaryOperator;

/**
 * BinaryOperatorインタフェースのの実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see BinaryOperator
 */
public final class Power implements BinaryOperator{

	
	/**
	 * コンストラクタ
	 */
	public Power(){
	}
	
	
	/**
	 * { @inheritDoc }
	 */
	@Override
	public int operate(int left,int right){
			int result = 1;                        // 一時変数の初期値を1にして
			for (int i = 0; i < right; i++) { // 右辺値の回数分だけ
				result *= left;                    // 左辺値をかけあわせて
			}
			return result;
	}


}