/*
 * Mul.java
 */
package ee57073;
import oop.BinaryOperator;
/**
 * BinaryOperatorインタフェースのの実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see BinaryOperator
 */
public final class Mul implements BinaryOperator{

	/**
	 * コンストラクタ
	 */
	public Mul(){
	}
		
	/**
	 * { @inheritDoc }
	 */
	@Override
	public int operate(int left,int right){
		return left * right;
	}

}