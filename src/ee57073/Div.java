/*
 * Div.java
 */
package ee57073;
import oop.BinaryOperator;

/**
 * BinaryOperatorインタフェースのの実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see BinaryOperator
 */
public final class Div implements BinaryOperator{

	/**
	 * コンストラクタ
	 */
	public Div(){
	}
		
	/**
	 * { @inheritDoc }
	 * @return result result
	 */
	@Override
	public int operate(final int left,final int right){
		return left / right;
	}

}