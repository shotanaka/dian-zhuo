/*
 * Add.java
 */
package ee57073;
import oop.BinaryOperator;

/**
 * BinaryOperatorインタフェースのの実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see BinaryOperator
 */
public final class Add implements BinaryOperator{

	/**
	 * コンストラクタ
	 */
	public Add(){
	}
		
	/**
	 * { @inheritDoc }
	 * @return result result
	 */
	@Override
	public int operate(final int left,final int right){
		return left + right;
	}

}