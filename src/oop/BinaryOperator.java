/*
 * BinaryOperator.java
 * ！！！！注意！！！！
 * このインタフェースを有効活用して電卓を改良してください．
 * このファイルは，一字一句たりとも書き換えないでください．
 */
package oop;

/**
 * 2項演算子を表すインタフェースです．
 * @author OOP_TA
 * @version 1.0.0
 */
public interface BinaryOperator {

    /**
     * 2項演算を行います．
     * @param left 左辺値
     * @param right 右辺値
     * @return 2項演算の結果
     * @throws IllegalArgumentException 引数が不正な場合
     */
    int operate(int left, int right) throws IllegalArgumentException;

}